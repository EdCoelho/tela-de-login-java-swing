package telaLogin;
import telaLogin.view.ViewLogin;
import telaLogin.model.Model;
import telaLogin.controller.Controller;

public class App {
	public static void main(String[] args) {
		ViewLogin view = new ViewLogin();
		Model model = new Model();
		Controller controller = new Controller(view, model);
	}
}