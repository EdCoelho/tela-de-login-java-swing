package telaLogin.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import telaLogin.model.Model;
import telaLogin.view.ViewLogin;

public class Controller {
	private ViewLogin view;
	private Model model;
	
	public Controller(ViewLogin view, Model model){
		this.view = view;
		this.model = model;
		
		checarLogin();
		this.view.setVisible(true);
	}
	
	public void checarLogin() {
		view.btnEntrar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String usuarioDigitado = view.getUsuario();
				String usuario = model.getUsuario();
				
				String senhaDigitada = view.getSenha();
				String senha = model.getSenha();
				
				if(senhaDigitada.equals(senha) && usuarioDigitado.equals(usuario)) {
					JOptionPane.showMessageDialog(null, "Login efetuado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(null, "Dados inválidos!", "Alerta!", JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
	}

}