package telaLogin.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class ViewLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;
	public JButton btnEntrar;

	/**
	 * Create the frame.
	 */
	public ViewLogin() {
		setTitle("Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(80, 60, 70, 15);
		contentPane.add(lblUsuario);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(80, 105, 70, 15);
		contentPane.add(lblSenha);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(145, 60, 114, 19);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(145, 105, 114, 19);
		contentPane.add(txtSenha);
		
		btnEntrar = new JButton("Logar");
		btnEntrar.setBounds(115, 155, 117, 25);
		contentPane.add(btnEntrar);
		getRootPane().setDefaultButton(btnEntrar);
	}
	
	public String getUsuario() {
		return txtUsuario.getText();
	}
	
	public String getSenha() {
		return new String(txtSenha.getPassword());
	}

}
